package mock;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;



public class MockAppUI extends JFrame {

    private boolean workspaceSelected = false;
    private File selectedWorkspace;

    public MockAppUI() {
        super("Mock Application");
        chooseWorkspace();
    }

    private void chooseWorkspace() {
        // Choose workspace dialog
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose workspace directory");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int userSelection = fileChooser.showOpenDialog(MockAppUI.this);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            selectedWorkspace = fileChooser.getSelectedFile();
            workspaceSelected = true;
            initializeUI();
        } else {
            // Handle if no workspace selected
            System.exit(0);
        }
    }

    private void initializeUI() {
        // Set default close operation
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set window size and position
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = screenSize.width / 2;
        int height = screenSize.height / 2;
        setSize(width, height);
        setLocationRelativeTo(null); // Center the window

        // Add logo
        ImageIcon logoIcon = new ImageIcon("path/to/your/logo.png"); // Specify path to your logo
        JLabel logoLabel = new JLabel(logoIcon);
        add(logoLabel, BorderLayout.NORTH);

        // Add title
        JLabel titleLabel = new JLabel("Mock Application");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        add(titleLabel, BorderLayout.CENTER);

        // Create menu bar
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        // Create File menu
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        // Add menu items to File menu
        JMenuItem openMenuItem = new JMenuItem("Open");
        openMenuItem.setIcon(new ImageIcon("path/to/openIcon.png")); // Specify path to your open icon
        fileMenu.add(openMenuItem);

        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.setIcon(new ImageIcon("path/to/exitIcon.png")); // Specify path to your exit icon
        fileMenu.add(exitMenuItem);

        // Add action listeners to menu items
        openMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Open file chooser dialog
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Choose exchange directory");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int userSelection = fileChooser.showOpenDialog(MockAppUI.this);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File selectedDirectory = fileChooser.getSelectedFile();
                    // Handle selected directory
                }
            }
        });

        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Exit application
                System.exit(0);
            }
        });

        // Set Look&Feel to Nimbus
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // Create and display the UI
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MockAppUI appUI = new MockAppUI();
                if (appUI.workspaceSelected) {
                    appUI.setVisible(true);
                }
            }
        });
    }
}
