package main.java.com.ubo.tp.message.main;



public class MainController implements MainObserver {
    private MainModel model;
    private MockView view;

    public MainController(MainModel model, MockView view) {
        this.model = model;
        this.view = view;
        this.model.addObserver(this);
    }

    public void init() {
        this.view.init();
        view.setVisible(true);
    }

    @Override
    public void update() {

    }
}
