package main.java.com.ubo.tp.message.main;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainModel implements MainObservableController{
    private File selectedWorkspace;
    private List<MainObserver> observers = new ArrayList<>();

    public void setSelectedWorkspace(File selectedWorkspace) {
        this.selectedWorkspace = selectedWorkspace;
        notifyObservers();
    }

    public File getSelectedWorkspace() {
        return selectedWorkspace;
    }

    @Override
    public void addObserver(MainObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(MainObserver observer) {
        observers.remove(observer);

    }

    @Override
    public void notifyObservers() {
        for (MainObserver observer : observers) {
            observer.update();
        }
    }
}
