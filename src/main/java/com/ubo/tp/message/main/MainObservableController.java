package main.java.com.ubo.tp.message.main;

public interface MainObservableController {
    void addObserver(MainObserver observer);
    void removeObserver(MainObserver observer);
    void notifyObservers();
}

interface MainObserver {
    void update();
}
