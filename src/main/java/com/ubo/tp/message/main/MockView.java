package main.java.com.ubo.tp.message.main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MockView extends JFrame {

    private MainModel model;

    public MockView(MainModel model) {
        super("Mock Application");
        this.model = model;
    }
    public void init() {
         chooseWorkspace();
    }

    private void chooseWorkspace() {
        // Choix du répertoire de travail
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choose workspace directory");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int userSelection = fileChooser.showOpenDialog(MockView.this);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File selectedWorkspace = fileChooser.getSelectedFile();
            model.setSelectedWorkspace(selectedWorkspace);
            //initializeUI();
        } else {
            System.exit(0);
        }
    }

    public void initializeUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = screenSize.width / 2;
        int height = screenSize.height / 2;
        setSize(width, height);
        setLocationRelativeTo(null);

        // Ajout du logo
        ImageIcon logoIcon = new ImageIcon("path/to/your/logo.png");
        JLabel logoLabel = new JLabel(logoIcon);
        add(logoLabel, BorderLayout.NORTH);

        // Ajout du titre
        JLabel titleLabel = new JLabel("Mock Application");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        add(titleLabel, BorderLayout.CENTER);

        // Création de la barre de menu
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        // Création du menu Fichier
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        // Ajout des éléments de menu au menu Fichier
        JMenuItem openMenuItem = new JMenuItem("Open");
        openMenuItem.setIcon(new ImageIcon("path/to/openIcon.png"));
        fileMenu.add(openMenuItem);

        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.setIcon(new ImageIcon("path/to/exitIcon.png"));
        fileMenu.add(exitMenuItem);

        // Ajout des écouteurs d'événements aux éléments de menu
        openMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Choose exchange directory");
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int userSelection = fileChooser.showOpenDialog(MockView.this);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File selectedDirectory = fileChooser.getSelectedFile();
                    // Gérer le répertoire sélectionné
                }
            }
        });

        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // Définir le Look&Feel à Nimbus
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
