package main.java.com.ubo.tp.message.main;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainModel model = new MainModel();
                MockView view = new MockView(model);
                MainController controller = new MainController(model, view);
                controller.init();
            }
        });
    }
}
