package main.java.com.ubo.tp.message.exampe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ViewController3 implements ActionListener {
    private BidirectionalNavigationExample frame;
    private View3 view3;
    private View2 view2;

    public ViewController3(BidirectionalNavigationExample frame, View3 view3, View2 view2) {
        this.frame = frame;
        this.view3 = view3;
        this.view2 = view2;

        view3.getBackButton().addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        frame.getCardLayout().show(frame.getContentPane(), "View2");
    }
}
