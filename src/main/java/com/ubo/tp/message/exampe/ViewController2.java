package main.java.com.ubo.tp.message.exampe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ViewController2 implements ActionListener {
    private BidirectionalNavigationExample frame;
    private View2 view2;
    private View1 view1;
    private View3 view3;

    public ViewController2(BidirectionalNavigationExample frame, View2 view2, View1 view1, View3 view3) {
        this.frame = frame;
        this.view2 = view2;
        this.view1 = view1;
        this.view3 = view3;

        view2.getBackButton().addActionListener(this);
        view2.getNextButton().addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == view2.getBackButton()) {
            frame.getCardLayout().show(frame.getContentPane(), "View1");
        } else if (e.getSource() == view2.getNextButton()) {
            frame.getCardLayout().show(frame.getContentPane(), "View3");
        }
    }
}