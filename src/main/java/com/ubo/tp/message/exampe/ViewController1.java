package main.java.com.ubo.tp.message.exampe;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ViewController1 implements ActionListener {
    private BidirectionalNavigationExample frame;
    private View1 view1;
    private View2 view2;

    public ViewController1(BidirectionalNavigationExample frame, View1 view1, View2 view2) {
        this.frame = frame;
        this.view1 = view1;
        this.view2 = view2;

        view1.getNextButton().addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        frame.getCardLayout().show(frame.getContentPane(), "View2");
    }
}
