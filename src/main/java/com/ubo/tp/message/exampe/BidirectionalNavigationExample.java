package main.java.com.ubo.tp.message.exampe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class BidirectionalNavigationExample extends JFrame {
    private JPanel contentPane;
    private CardLayout cardLayout;

    public BidirectionalNavigationExample() {
        setTitle("Bidirectional Navigation Example");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);

        // Create content pane with CardLayout
        contentPane = new JPanel();
        cardLayout = new CardLayout();
        contentPane.setLayout(cardLayout);
        getContentPane().add(contentPane, BorderLayout.CENTER);

        // Create views and controllers
        View1 view1 = new View1();
        View2 view2 = new View2();
        View3 view3 = new View3();

        ViewController1 controller1 = new ViewController1(this, view1, view2);
        ViewController2 controller2 = new ViewController2(this, view2, view1, view3);
        ViewController3 controller3 = new ViewController3(this, view3, view2);

        contentPane.add(view1, "View1");
        contentPane.add(view2, "View2");
        contentPane.add(view3, "View3");

        // Set initial view
        cardLayout.show(contentPane, "View1");
    }

    public CardLayout getCardLayout() {
        return cardLayout;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                BidirectionalNavigationExample frame = new BidirectionalNavigationExample();
                frame.setVisible(true);
            }
        });
    }
}