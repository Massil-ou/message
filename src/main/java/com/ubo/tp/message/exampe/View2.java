package main.java.com.ubo.tp.message.exampe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
class View2 extends JPanel {
    private JButton backButton2;
    private JButton nextButton2;

    public View2() {
        setBackground(Color.BLUE);
        backButton2 = new JButton("Back");
        nextButton2 = new JButton("Next");
        add(backButton2);
        add(nextButton2);
    }

    public JButton getBackButton() {
        return backButton2;
    }

    public JButton getNextButton() {
        return nextButton2;
    }
}