package main.java.com.ubo.tp.message.exampe;

import javax.swing.*;
import java.awt.*;

class View3 extends JPanel {
    private JButton backButton3;

    public View3() {
        setBackground(Color.GREEN);
        backButton3 = new JButton("Back");
        add(backButton3);
    }

    public JButton getBackButton() {
        return backButton3;
    }
}