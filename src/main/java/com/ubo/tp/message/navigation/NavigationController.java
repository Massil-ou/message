package main.java.com.ubo.tp.message.navigation;

public interface NavigationController {
    void navigateToLogin();
    void navigateToSignIn();
}
