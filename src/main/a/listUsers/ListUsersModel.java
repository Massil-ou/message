package main.a.listUsers;



import main.a.profile.ProfileObservableController;
import main.java.com.ubo.tp.message.datamodel.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListUsersModel implements ListUsersObservableController {


    protected User user;
    private Map<User, Boolean> userMap; // Utilisation d'un Map<User, Boolean> pour stocker les utilisateurs et leurs états (true pour ajouter, false pour supprimer)
    private String userTag;
    private String userTagRem;

    private List<ListUsersObserver> observers = new ArrayList<>();
    public ListUsersModel() {
    }

    public void setUserTag(String userTag){
        this.userTag=userTag;
        up();
    }
    public String getUserTag(){
        return this.userTag;
    }
    public void setUserTagRem(String userTagRem){
        this.userTagRem=userTagRem;
        rm();
    }
    public String getUserTagRem(){
        return this.userTagRem;
    }


    public void setUsersMap(Map<User, Boolean> users) {
        this.userMap = users;
    }
    public Map<User, Boolean> getUsersMap() {
        return userMap;
    }



    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }


    public User getUserFromTag(String tag) {
        for (Map.Entry<User, Boolean> entry : userMap.entrySet()) {
            User currentUser = entry.getKey();
            if (currentUser.getUserTag().equals(tag)) {
                return currentUser;
            }
        }
        return null;
    }
    public void up(){
        update();
    }

    public void rm() {
        supp();
    }

    @Override
    public void addObserver(ListUsersObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(ListUsersObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (ListUsersObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }

    @Override
    public void update() {
        for (ListUsersObserver observer : observers) {
            observer.update();
        }
    }

    @Override
    public void supp() {
        for (ListUsersObserver observer : observers) {
            observer.supp();
        }
    }

}

