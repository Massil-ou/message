package main.a.listUsers;
import main.a.NavigationModel;
import main.java.com.ubo.tp.message.datamodel.User;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;

public class ListUsersView extends JPanel {
    private ListUsersModel listUsersModel;

    public ListUsersView(NavigationModel model, ListUsersModel listUsersModel) {
        this.listUsersModel = listUsersModel;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        for (Map.Entry<User, Boolean> entry : listUsersModel.getUsersMap().entrySet()) {
            User user = entry.getKey();
            boolean addMode = entry.getValue();
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JButton actionButton = new JButton(addMode ? "Ajouter" : "Supprimer");
            actionButton.addActionListener(new ActionButtonListener(user, addMode));
            panel.add(new JLabel(user.getUserTag()));
            panel.add(actionButton);
            add(panel);
        }

        JButton deconnexionButton = new JButton("Déconnecter");
        deconnexionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("LoginView");
            }
        });
        add(deconnexionButton);

        JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("MenuView");
            }
        });
        add(backButton);
    }

    private class ActionButtonListener implements ActionListener {
        private User user;
        private boolean addMode;

        public ActionButtonListener(User user, boolean addMode) {
            this.user = user;
            this.addMode = addMode;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Set<String> follows = listUsersModel.getUser().getFollows();
            String userTag = user.getUserTag();
            if (addMode) {
                if (!follows.contains(userTag)) {
                    listUsersModel.setUserTag(userTag);
                }
            } else {
                if (follows.contains(userTag)) {
                    listUsersModel.setUserTagRem(userTag);

                }
            }
        }
    }
    public void refreshUserList(Map<User, Boolean> users) {
        // Supprimer tous les composants précédents de la liste d'utilisateurs
        Component[] components = getComponents();
        for (Component component : components) {
            if (component instanceof JPanel) {
                remove(component);
            }
        }

        // Ajouter les nouveaux composants pour chaque utilisateur dans la liste fournie
        for (Map.Entry<User, Boolean> entry : users.entrySet()) {
            User user = entry.getKey();
            boolean addMode = entry.getValue();
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JButton actionButton = new JButton(addMode ? "Ajouter" : "Supprimer");
            actionButton.addActionListener(new ActionButtonListener(user, addMode));
            panel.add(new JLabel(user.getUserTag()));
            panel.add(actionButton);
            add(panel);
        }

        // Actualiser la vue
        revalidate();
        repaint();
    }

}
