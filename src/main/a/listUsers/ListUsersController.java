package main.a.listUsers;


import main.a.NavigationModel;
import main.a.signin.SignInView;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ListUsersController implements ListUsersObserver {
    private NavigationModel model;
    private ListUsersModel listUsersModel;
    private ListUsersView listUsersView;
    protected IDatabase mDatabase;

    protected EntityManager mEntityManager;

    public ListUsersController(NavigationModel model, ListUsersModel loginmodel, IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.listUsersModel=loginmodel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.listUsersModel.addObserver(this);
    }
    public void setView(ListUsersView listUsersView){
        this.listUsersView=listUsersView;
    }

    public ListUsersView getView(){
        return listUsersView;
    }

    @Override
    public void navigateToSigninView() {
        model.navigateTo("SignInView");
    }

    @Override
    public void update() {
        listUsersModel.getUser().addFollowing(listUsersModel.getUserTag());

        Set<User> users = mDatabase.getUsers();
        Map<User, Boolean> userBooleanMap = new HashMap<>();

        // Initialiser la map avec chaque utilisateur associé à true
        for (User user : users) {
            userBooleanMap.put(user, true);
        }

        // Mettre à jour la map pour les utilisateurs présents dans la liste des followers avec false
        Set<String> followers = listUsersModel.user.getFollows();
        for (String follower : followers) {
            User res = listUsersModel.getUserFromTag(follower);
            if (res != null) {
                userBooleanMap.put(res, false);
            }
        }
        listUsersModel.setUsersMap(userBooleanMap);
        listUsersView.refreshUserList(userBooleanMap);
    }

    @Override
    public void supp() {
         listUsersModel.getUser().removeFollowing(listUsersModel.getUserTagRem());

        Set<User> users = mDatabase.getUsers();
        Map<User, Boolean> userBooleanMap = new HashMap<>();

        // Initialiser la map avec chaque utilisateur associé à true
        for (User user : users) {
            userBooleanMap.put(user, true);
        }

        // Mettre à jour la map pour les utilisateurs présents dans la liste des followers avec false
        Set<String> followers = listUsersModel.user.getFollows();
        for (String follower : followers) {
            User res = listUsersModel.getUserFromTag(follower);
            if (res != null) {
                userBooleanMap.put(res, false);
            }
        }
        listUsersModel.setUsersMap(userBooleanMap);
        listUsersView.refreshUserList(userBooleanMap);
    }

}