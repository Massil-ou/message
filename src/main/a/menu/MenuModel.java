package main.a.menu;


import main.java.com.ubo.tp.message.datamodel.User;

import java.util.ArrayList;
import java.util.List;

public class MenuModel implements MenuObservableController {

    protected String mUserNom;
    protected String mUserTag;
    protected User user;

    private boolean isValid = false;

    private List<MenuObserver> observers = new ArrayList<>();

    public MenuModel(String userTag, String userPassword) {
        mUserTag = userTag;
        mUserNom = userPassword;
    }
    public void setUser(User user) {
        this.user = user;
    }

    // Méthode pour obtenir l'état de validation de l'inscription
    public User getUser() {
        return user;
    }
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    // Méthode pour obtenir l'état de validation de l'inscription
    public boolean isValid() {
        return isValid;
    }
    public void NavigateToLoginView() {
        notifyObservers();
    }
    public String getUserTag() {
        return mUserTag;
    }
    public void setUserTag(String userTag) {
        mUserTag = userTag;
    }
    public void setModel(String userTag,String userNom) {
        mUserTag = userTag;
        mUserNom = userNom;
        Login();
    }
    public String getUserNom() {
        return mUserNom;
    }
    public void setUserNom(String userPassword) {
        mUserNom = userPassword;
    }

    @Override
    public void addObserver(MenuObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(MenuObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (MenuObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }

    @Override
    public void Login() {
        for (MenuObserver observer : observers) {
            observer.Login();
        }
    }

    @Override
    public void navigateToSigninView() {
        for (MenuObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }
}

