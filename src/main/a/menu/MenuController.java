package main.a.menu;

import main.a.NavigationModel;
import main.a.signin.SignInView;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.User;

public class MenuController implements MenuObserver {
    private NavigationModel model;
    private MenuModel loginmodel;
    private MenuView menuView;
    protected IDatabase mDatabase;

    protected EntityManager mEntityManager;

    public MenuController(NavigationModel model, MenuModel loginmodel, IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.loginmodel=loginmodel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.loginmodel.addObserver(this);
    }
    public void setView(MenuView menuView){
        this.menuView=menuView;
    }

    public MenuView getView(){
        return menuView;
    }

    public void navigateToSigninView2() {
        model.navigateTo("LoginView");
    }



    @Override
    public void Login() {
        String userTag = loginmodel.getUserTag();
        String userNom = loginmodel.getUserNom();

        if (isUserCredentialsValid(userTag, userNom)) {
            System.out.println("Connexion réussie");
            loginmodel.setValid(true); // L'inscription est valide
        } else {
            System.out.println("Informations de connexion invalides");
            loginmodel.setValid(false); // L'inscription est valide
        }
    }
    private boolean isUserCredentialsValid(String userTag, String userNom) {
        for (User user : this.mDatabase.getUsers()) {
            if (user.getUserTag().equals(userTag) && user.getName().equals(userNom)) {
                return true; // Le tag existe déjà
            }
        }
        return false;
    }

    @Override
    public void navigateToSigninView() {
        model.navigateTo("SignInView");
    }




}