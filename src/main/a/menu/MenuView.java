package main.a.menu;

import main.a.NavigationModel;
import main.java.com.ubo.tp.message.datamodel.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

public class MenuView extends JPanel {
    private MenuModel menuModel;
    private JTextField userTagTextField;
    private JTextField userNomTextField;
    private Set<User> users;
    private JTextField searchField;

    public MenuView(NavigationModel model, MenuModel menuModel) {
        this.menuModel = menuModel;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); // Utilisation de BoxLayout pour organiser les éléments verticalement
        setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        // Création des champs texte pour afficher les informations du modèle
        userTagTextField = new JTextField(20);
        userTagTextField.setEditable(false); // Rendre le champ non modifiable
        userNomTextField = new JTextField(20);
        userNomTextField.setEditable(false); // Rendre le champ non modifiable

        // Création des étiquettes pour les champs texte
        JLabel userTagLabel = new JLabel("User Tag:");
        JLabel userNomLabel = new JLabel("User Nom:");

        // Ajout des étiquettes et des champs texte à la vue
        add(userTagLabel);
        add(userTagTextField);
        add(userNomLabel);
        add(userNomTextField);

        // Ajout d'un champ de texte pour la recherche d'utilisateur
        searchField = new JTextField(20);
        add(searchField);

        // Ajout d'un bouton "Déconnecter"
        JButton deconnexionButton = new JButton("Déconnecter");
        deconnexionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Naviguer vers la vue de connexion (LoginView)
                model.navigateTo("LoginView");
            }
        });
        add(deconnexionButton);

        // Ajout d'un bouton "Profil" pour afficher les informations du modèle dans une boîte de dialogue
        JButton profilButton = new JButton("Profil");
        profilButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("Profile");
            }
        });
        add(profilButton);

        JButton message = new JButton("Message");
        message.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("Message");
            }
        });
        add(message);

        JButton filtermessage = new JButton("Filter Message");
        filtermessage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("FilterMessage");
            }
        });
        add(filtermessage);

        // Ajout d'un bouton "Liste d'utilisateurs"
        JButton userListButton = new JButton("Liste d'utilisateurs");
        userListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("ListeUsers");
                //showUserListDialog();
            }
        });
        add(userListButton);

        // Ajout d'un bouton "Rechercher utilisateur"
        JButton searchButton = new JButton("Rechercher utilisateur");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchUser();
            }
        });
        add(searchButton);
        JButton followersButton = new JButton("Liste des Followers");
        followersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showFollowersList();
            }
        });
        add(followersButton);

        add(Box.createVerticalGlue());

        updateFields();
    }

    private void showFollowersList() {
        Set<String> followers = menuModel.getUser().getFollows();
        JDialog dialog = new JDialog((JFrame) SwingUtilities.getWindowAncestor(MenuView.this), "Liste des Followers", true); // Création d'un dialogue modale
        JPanel panel = new JPanel(new GridLayout(followers.size() + 1, 2)); // Panel pour afficher les followers et les boutons

        // Titres
        panel.add(new JLabel("User Tag"));
        panel.add(new JLabel("Supprimer le follower"));

        for (String follower : followers) {
            JButton removeButton = new JButton("Supprimer");
            removeButton.addActionListener(new RemoveFollowerActionListener(follower, dialog));
            panel.add(new JLabel(follower));
            panel.add(removeButton);
        }

        dialog.add(panel);
        dialog.pack();
        dialog.setVisible(true);
    }


    private class RemoveFollowerActionListener implements ActionListener {
        private String follower;
        private JDialog dialog;

        public RemoveFollowerActionListener(String follower, JDialog dialog) {
            this.follower = follower;
            this.dialog = dialog;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            menuModel.getUser().removeFollowing(follower);
            JOptionPane.showMessageDialog(MenuView.this, "Le follower " + follower + " a été supprimé.", "Suppression de follower", JOptionPane.INFORMATION_MESSAGE);

            // Fermer le dialogue après la suppression
            dialog.dispose();
        }
    }


    private void updateFields() {
        userTagTextField.setText(menuModel.getUserTag());
        userNomTextField.setText(menuModel.getUserNom());
    }

    private void showProfileDialog() {
        JOptionPane.showMessageDialog(MenuView.this, "User Tag: " + menuModel.getUserTag() + "\nUser Nom: " + menuModel.getUserNom(), "Profil", JOptionPane.INFORMATION_MESSAGE);
    }

    private void showUserListDialog() {
        JPanel panel = new JPanel(new GridLayout(users.size() + 1, 2)); // Panel pour afficher les utilisateurs et les boutons
        panel.add(new JLabel("User Tag"));
        panel.add(new JLabel("Ajouter en tant que follower"));

        for (User user : users) {
            if(!user.equals(menuModel.getUser())){
                JButton addButton = new JButton("Ajouter");
                addButton.addActionListener(new AddFollowerActionListener(user));
                panel.add(new JLabel(user.getUserTag()));
                panel.add(addButton);
            }
        }

        JOptionPane.showMessageDialog(MenuView.this, panel, "Liste d'utilisateurs", JOptionPane.INFORMATION_MESSAGE);
    }
    // Classe interne pour gérer l'action de l'ajout de follower lorsque le bouton est cliqué
    private class AddFollowerActionListener implements ActionListener {
        private User usern;

        public AddFollowerActionListener(User user) {
            this.usern = user;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Set<String> follows = menuModel.getUser().getFollows();
            String userTagToAdd = usern.getUserTag();

            if (!follows.contains(userTagToAdd)) {
                menuModel.getUser().addFollowing(userTagToAdd);
                JOptionPane.showMessageDialog(MenuView.this, "L'utilisateur " + userTagToAdd + " a été ajouté en tant que follower.", "Ajout de follower", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(MenuView.this, "L'utilisateur " + userTagToAdd + " est déjà un follower.", "Ajout de follower", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }


    // Méthode pour rechercher un utilisateur
    private void searchUser() {
        String searchQuery = searchField.getText().trim();
        if (!searchQuery.isEmpty()) {
            // Effectuer la recherche dans la liste des utilisateurs
            StringBuilder searchResult = new StringBuilder("Résultat de la recherche :\n");
            for (User user : users) {
                if (user.getUserTag().equalsIgnoreCase(searchQuery)) {

                    searchResult.append("User Tag: ").append(user.getUserTag()).append(", User Nom: ").append(user.getUserTag()).append("\n");
                }
            }
            JOptionPane.showMessageDialog(MenuView.this, searchResult.toString(), "Résultat de la recherche", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(MenuView.this, "Veuillez saisir un nom d'utilisateur à rechercher.", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }
}
