package main.a.menu;


public interface MenuObservableController {

    void addObserver(MenuObserver observer);
    void removeObserver(MenuObserver observer);
    void notifyObservers();
    void Login();
    void navigateToSigninView();

}

interface MenuObserver {
    void Login();
    void navigateToSigninView();

}
