package main.a.login;


import main.java.com.ubo.tp.message.datamodel.User;

import java.util.ArrayList;
import java.util.List;

public class LoginModel implements LoginObservableController {

    protected String mUserNom;
    protected String mUserTag;
    protected User user;

    private boolean isValid = false;

    private List<LoginObserver> observers = new ArrayList<>();

    public LoginModel(String userTag, String userPassword) {
        mUserTag = userTag;
        mUserNom = userPassword;
    }

    public void setUser(User user) {
        this.user = user;
    }

    // Méthode pour obtenir l'état de validation de l'inscription
    public User getUser() {
        return user;
    }
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    // Méthode pour obtenir l'état de validation de l'inscription
    public boolean isValid() {
        return isValid;
    }
    public void NavigateToLoginView() {
        notifyObservers();
    }
    public String getUserTag() {
        return mUserTag;
    }
    public void setUserTag(String userTag) {
        mUserTag = userTag;
    }
    public void setModel(String userTag,String userNom) {
        mUserTag = userTag;
        mUserNom = userNom;
        Login();
    }
    public String getUserNom() {
        return mUserNom;
    }
    public void setUserNom(String userPassword) {
        mUserNom = userPassword;
    }

    @Override
    public void addObserver(LoginObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(LoginObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (LoginObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }

    @Override
    public void Login() {
        for (LoginObserver observer : observers) {
            observer.Login();
        }
    }

    @Override
    public void navigateToSigninView() {
        for (LoginObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }
}

