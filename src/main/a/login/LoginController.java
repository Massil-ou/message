package main.a.login;

import main.a.NavigationModel;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.User;
import main.java.com.ubo.tp.message.main.MainModel;

public class LoginController implements LoginObserver{
    private NavigationModel model;
    private LoginModel loginmodel;
    protected IDatabase mDatabase;
    LoginView loginView;
    protected EntityManager mEntityManager;

    public LoginController(NavigationModel model,LoginModel loginmodel,IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.loginmodel=loginmodel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.loginmodel.addObserver(this);
    }

    public void setView(LoginView loginView){
        this.loginView=loginView;
    }

    public LoginView getView(){
        return loginView;
    }

    public void navigateToSigninView2() {
        model.navigateTo("LoginView");
    }

    public void navigateToMenu() {
        model.navigateTo("MenuView");
    }


    @Override
    public void Login() {
        String userTag = loginmodel.getUserTag();
        String userNom = loginmodel.getUserNom();

        if (isUserCredentialsValid(userTag, userNom)) {
            System.out.println("Connexion réussie");
            loginmodel.setValid(true); // L'inscription est valide
            navigateToMenu();
        } else {
            System.out.println("Informations de connexion invalides");
            loginmodel.setValid(false); // L'inscription est valide
        }
    }
    private boolean isUserCredentialsValid(String userTag, String userNom) {
        for (User user : this.mDatabase.getUsers()) {
            if (user.getUserTag().equals(userTag) && user.getName().equals(userNom)) {
                loginmodel.setUser(user);
                return true; // Le tag existe déjà
            }
        }
        return false;
    }

    @Override
    public void navigateToSigninView() {
        model.navigateTo("SignInView");
    }




}