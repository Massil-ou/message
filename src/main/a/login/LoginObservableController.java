package main.a.login;


public interface LoginObservableController {

    void addObserver(LoginObserver observer);
    void removeObserver(LoginObserver observer);
    void notifyObservers();
    void Login();
    void navigateToSigninView();

}

interface LoginObserver {
    void Login();
    void navigateToSigninView();

}
