package main.a.login;

import main.a.NavigationModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginView extends JPanel {
    private JTextField userTagField;
    private JTextField userNomField;
    private LoginModel loginModel;
    private JTextField resultTextField;

    public LoginView(NavigationModel model, LoginModel loginModel) {
        this.loginModel = loginModel;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(Component.CENTER_ALIGNMENT);

        userTagField = new JTextField(20);
        userNomField = new JTextField(20);

        JButton signInButton = new JButton("Sign in");
        JButton loginButton = new JButton("Login");

        resultTextField = new JTextField(20);
        resultTextField.setEditable(false);

        signInButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loginModel.NavigateToLoginView();

                //model.navigateTo("SingInView");
            }
        });

        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String userTag = userTagField.getText();
                String userNom = userNomField.getText();
                if (userTag.length() <= 0 || userNom.length() <= 0) {
                    JOptionPane.showMessageDialog(LoginView.this,
                            "Username or tag cannot be empty.",
                            "Sign in Information",
                            JOptionPane.INFORMATION_MESSAGE);
                }else {
                    loginModel.setModel(userTag, userNom);
                    // Vérifie si l'inscription est valide
                    if (loginModel.isValid()) {
                        resultTextField.setText("Connexion valide"); // Affiche le résultat dans le champ de texte
                    } else {
                        resultTextField.setText("Connexion invalide"); // Affiche le résultat dans le champ de texte
                    }
                }
            }
        });

        // Ajout d'espaces flexibles pour centrer verticalement les éléments
        add(Box.createVerticalGlue());

        // Ajout des éléments (Nom)
        JPanel nomPanel = new JPanel();
        nomPanel.setLayout(new BoxLayout(nomPanel, BoxLayout.X_AXIS)); // Utilisation de BoxLayout pour organiser les éléments horizontalement
        nomPanel.setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        nomPanel.add(new JLabel("Nom:"));
        nomPanel.add(userNomField);

        add(nomPanel);

        // Ajout des éléments (Tag)
        JPanel tagPanel = new JPanel();
        tagPanel.setLayout(new BoxLayout(tagPanel, BoxLayout.X_AXIS)); // Utilisation de BoxLayout pour organiser les éléments horizontalement
        tagPanel.setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        tagPanel.add(new JLabel("Tag:"));
        tagPanel.add(userTagField);

        add(tagPanel);

        // Ajout d'un espacement vertical entre les champs de saisie et les boutons
        add(Box.createVerticalStrut(10));

        // Ajout des boutons
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        buttonPanel.add(signInButton);
        buttonPanel.add(Box.createHorizontalStrut(10)); // Espacement horizontal
        buttonPanel.add(loginButton);

        add(buttonPanel);
        add(resultTextField);

        // Ajout d'un espace flexible pour centrer verticalement les boutons
        add(Box.createVerticalGlue());
    }
}
