package main.a.signin;

import main.a.NavigationModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SignInView extends JPanel {
    private SigninModel signinModel;
    private JTextField resultTextField;

    public SignInView(NavigationModel model,SigninModel signinModel) {
        this.signinModel = signinModel;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel userTagLabel = new JLabel("Tag:");
        JTextField userTagField = new JTextField(20);
        JLabel userNomLabel = new JLabel("Nom:");
        JTextField userNomField = new JTextField(20);
        JButton signUpButton = new JButton("Sign Up");

        resultTextField = new JTextField(20);
        resultTextField.setEditable(false);

        JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                signinModel.NavigateToSignInView();
            }
        });

        signUpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Récupération des valeurs saisies dans les champs
                String userTag = userTagField.getText();
                String userNom = userNomField.getText();
                if (userTag.length() <= 0 || userNom.length() <= 0) {
                    JOptionPane.showMessageDialog(SignInView.this,
                            "Username or tag cannot be empty.",
                            "Sign in Information",
                            JOptionPane.INFORMATION_MESSAGE);
                } else {
                    signinModel.setModel(userTag, userNom);

                    // Vérifie si l'inscription est valide
                    if (signinModel.isValid()) {
                        resultTextField.setText("Inscription valide"); // Affiche le résultat dans le champ de texte
                    } else {
                        resultTextField.setText("Inscription invalide"); // Affiche le résultat dans le champ de texte
                    }
                    signinModel.setValid(false);
                }
            }
        });

        // Ajout d'espaces flexibles pour centrer verticalement les éléments
        add(Box.createVerticalGlue());

        // Ajout des éléments (Nom)
        JPanel nomPanel = new JPanel();
        nomPanel.setLayout(new BoxLayout(nomPanel, BoxLayout.X_AXIS)); // Utilisation de BoxLayout pour organiser les éléments horizontalement
        nomPanel.setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        nomPanel.add(userNomLabel);
        nomPanel.add(userNomField);

        add(nomPanel);

        // Ajout des éléments (Tag)
        JPanel tagPanel = new JPanel();
        tagPanel.setLayout(new BoxLayout(tagPanel, BoxLayout.X_AXIS)); // Utilisation de BoxLayout pour organiser les éléments horizontalement
        tagPanel.setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        tagPanel.add(userTagLabel);
        tagPanel.add(userTagField);

        add(tagPanel);

        // Ajout d'un espacement vertical entre les champs de saisie et les boutons
        add(Box.createVerticalStrut(10));

        // Ajout des boutons
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT); // Centrage horizontal du JPanel

        buttonPanel.add(signUpButton);
        buttonPanel.add(Box.createHorizontalStrut(10)); // Espacement horizontal
        buttonPanel.add(backButton);

        add(buttonPanel);
        add(resultTextField);
        // Ajout d'un espace flexible pour centrer verticalement les boutons
        add(Box.createVerticalGlue());
    }
}
