package main.a.signin;

import main.a.NavigationModel;
import main.a.login.LoginView;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.User;

import java.util.HashSet;
import java.util.Random;
import java.util.UUID;

public class SignInController implements SignInObserver{
    private NavigationModel model;
    private SigninModel signinModel;
    private SignInView signInView;

    protected IDatabase mDatabase;

    protected EntityManager mEntityManager;

    public SignInController(NavigationModel model,SigninModel signinModel,IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.signinModel=signinModel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.signinModel.addObserver(this);
    }
    public void setView(SignInView signInView){
        this.signInView=signInView;
    }

    public SignInView getView(){
        return signInView;
    }




    @Override
    public void Signin() {
        int randomInt = new Random().nextInt(99999);
        String userName = "MockUser" + randomInt;
        User newUser = new User(UUID.randomUUID(), signinModel.mUserTag, "--", signinModel.mUserNom, new HashSet<>(), "");

        if (!isUserTagExists(newUser.getUserTag())) {
            this.mDatabase.addUser(newUser);
            signinModel.setValid(true); // L'inscription est valide


        } else {
            signinModel.setValid(false); // L'inscription est valide

        }
    }
    private boolean isUserTagExists(String userTag) {
        // Parcours de la Set pour vérifier si le tag existe déjà
        for (User user : this.mDatabase.getUsers()) {
            if (user.getUserTag().equals(userTag)) {
                return true; // Le tag existe déjà
            }
        }
        return false; // Le tag n'existe pas encore
    }

    @Override
    public void navigateToLoginView() {
        model.navigateTo("LoginView");

    }
    public void navigateToLoginView2() {
        model.navigateTo("LoginView");
    }
}