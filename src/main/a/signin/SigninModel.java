package main.a.signin;
import java.util.ArrayList;
import java.util.List;

public class SigninModel  implements SignInObservableController{

    protected String mUserNom;
    protected String mUserTag;
    private List<SignInObserver> observers = new ArrayList<>();

    private boolean isValid = false;

    public SigninModel(String userTag, String userPassword) {
        mUserTag = userTag;
        mUserNom = userPassword;
    }
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    // Méthode pour obtenir l'état de validation de l'inscription
    public boolean isValid() {
        return isValid;
    }

    public String getUserTag() {
        return mUserTag;
    }
    public void setUserTag(String userTag) {
        mUserTag = userTag;
    }
    public void setModel(String userTag,String userNom) {
        mUserTag = userTag;
        mUserNom = userNom;
        Signin();
    }
    public String getUserNom() {
        return mUserNom;
    }
    public void setUserNom(String userPassword) {
        mUserNom = userPassword;
    }

    public void NavigateToSignInView() {
        notifyObservers();
    }


    @Override
    public void addObserver(SignInObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(SignInObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (SignInObserver observer : observers) {
            observer.navigateToLoginView();
        }

    }

    @Override
    public void Signin() {
        for (SignInObserver observer : observers) {
            observer.Signin();
        }
    }
}
