package main.a;

import main.java.com.ubo.tp.message.datamodel.User;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;


public interface MainObserver {
    void update(Observable o, Object arg);

    void update(Observable o, Object arg, User user);

}
