package main.a;

import main.a.filterMessage.FilterMessageController;
import main.a.filterMessage.FilterMessageModel;
import main.a.filterMessage.FilterMessageView;
import main.a.listUsers.ListUsersController;
import main.a.listUsers.ListUsersModel;
import main.a.listUsers.ListUsersView;
import main.a.login.LoginController;
import main.a.login.LoginModel;
import main.a.login.LoginView;
import main.a.menu.MenuController;
import main.a.menu.MenuModel;
import main.a.menu.MenuView;
import main.a.message.MessageController;
import main.a.message.MessageModel;
import main.a.message.MessageView;
import main.a.profile.ProfileController;
import main.a.profile.ProfileModel;
import main.a.profile.ProfileView;
import main.a.signin.SignInController;
import main.a.signin.SignInView;
import main.a.signin.SigninModel;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.User;

import javax.swing.*;
import java.awt.*;
import java.util.*;

// Modèle Observable
public class NavigationModel extends Observable {
    public void navigateTo(String viewName) {
        setChanged();
        notifyObservers(viewName);
    }
}

class MainFrame extends JFrame implements Observer {
    private JPanel currentView;
    private NavigationModel model;
    private LoginController loginController;
    private SignInController signInController;
    private MenuController menuController;
    private ProfileController profileController;
    private MessageController messageController;
    private ListUsersController listUsersController;
    private FilterMessageController filterMessageController;


    LoginModel user = new LoginModel("", "");
    SigninModel userSignin = new SigninModel("", "");
    MenuModel menuModel = new MenuModel("", "");
    MessageModel messageModel;
    ProfileModel profileModel = new ProfileModel();
    FilterMessageModel filterMessageModel;

    ListUsersModel listUsersModel = new ListUsersModel();

    protected IDatabase mDatabase;
    protected EntityManager mEntityManager;

    MainFrame(NavigationModel model, IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        messageModel = new MessageModel("", "", database.getMessages());
        filterMessageModel = new FilterMessageModel("", "", database.getMessages());
        model.addObserver(this);
        setTitle("Main View");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1000, 500);
        setLocationRelativeTo(null);
        currentView = new JPanel();
        getContentPane().add(currentView, BorderLayout.CENTER);
        loginController = new LoginController(model, user, database, entityManager);
        signInController = new SignInController(model, userSignin, database, entityManager);
        menuController = new MenuController(model, menuModel, database, entityManager);
        profileController = new ProfileController(model, profileModel, database, entityManager);
        listUsersController = new ListUsersController(model, listUsersModel, database, entityManager);
        messageController = new MessageController(model, messageModel, database, entityManager);
        filterMessageController  = new FilterMessageController(model, filterMessageModel, database, entityManager);
        loginController.navigateToSigninView2();
    }

    private void navigateTo(String viewName) {
        currentView.removeAll();

        if (viewName.equals("LoginView")) {
            LoginView loginView = new LoginView(model, user);
            loginController.setView(loginView);
            currentView.add(loginController.getView());
        } else if (viewName.equals("SignInView")) {
            SignInView signInView = new SignInView(model, userSignin);
            signInController.setView(signInView);
            currentView.add(signInController.getView());
        } else if (viewName.equals("MenuView")) {
            menuModel.setModel(user.getUserTag(), user.getUserNom());
            menuModel.setUser(user.getUser());
            MenuView menuView = new MenuView(model, menuModel);
            menuController.setView(menuView);
            currentView.add(menuController.getView());
        } else if (viewName.equals("Profile")) {
            profileModel.setUser(user.getUser());
            ProfileView profileView = new ProfileView(model, profileModel);
            profileController.setView(profileView);
            currentView.add(profileController.getView());
        } else if (viewName.equals("ListeUsers")) {
            listUsersModel.setUser(user.getUser());

            Set<User> users = mDatabase.getUsers();
            Map<User, Boolean> userBooleanMap = new HashMap<>();

            // Initialiser la map avec chaque utilisateur associé à true
            for (User user : users) {
                userBooleanMap.put(user, true);
            }
            // Mettre à jour la map pour les utilisateurs présents dans la liste des followers avec false
            Set<String> followers = user.getUser().getFollows();
            for (String follower : followers) {
                User res = listUsersModel.getUserFromTag(follower);
                if (res != null) {
                    userBooleanMap.put(res, false);
                }
            }

            listUsersModel.setUsersMap(userBooleanMap);
            ListUsersView listUsersView = new ListUsersView(model, listUsersModel);
            listUsersController.setView(listUsersView);
            currentView.add(listUsersController.getView());
        } else if (viewName.equals("Message")) {
            messageModel.setUser(user.getUser());
            messageModel.setUsers(mDatabase.getUsers());
            MessageView messageView = new MessageView(model, messageModel);
            messageController.setView(messageView);
            currentView.add(messageController.getView());
        }else if (viewName.equals("FilterMessage")) {
            filterMessageModel.setUser(user.getUser());
            filterMessageModel.setUsers(mDatabase.getUsers());
            FilterMessageView filtermessageView = new FilterMessageView(model, filterMessageModel);
            filterMessageController.setView(filtermessageView);
            currentView.add(filterMessageController.getView());
        }

        currentView.revalidate();
        currentView.repaint();
    }
    public Map<User, Boolean> updateUserMap(Set<User> users, User currentUser, Map<User, Boolean> userBooleanMap, ListUsersModel listUsersModel) {
        for (User user : users) {
            userBooleanMap.put(user, true);
        }

        Set<String> followers = currentUser.getFollows();
        for (String follower : followers) {
            User res = listUsersModel.getUserFromTag(follower);
            if (res != null) {
                userBooleanMap.put(res, false);
            }
        }
        return userBooleanMap;
    }

    @Override
    public void update(Observable o, Object arg) {
        String viewName = (String) arg;
        navigateTo(viewName);
    }
}

// Contrôleur de la vue
class MainController {
    protected IDatabase mDatabase;
    protected EntityManager mEntityManager;
    MainController(NavigationModel model, IDatabase mDatabase, EntityManager mEntityManager) {
        MainFrame frame = new MainFrame(model, mDatabase, mEntityManager);
        frame.setVisible(true);
    }
}
