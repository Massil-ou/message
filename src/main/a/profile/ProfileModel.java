package main.a.profile;


import main.java.com.ubo.tp.message.datamodel.User;

import java.util.ArrayList;
import java.util.List;

public class ProfileModel implements ProfileObservableController {


    protected User user;
    private List<ProfileObserver> observers = new ArrayList<>();

    public ProfileModel() {
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public void addObserver(ProfileObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(ProfileObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (ProfileObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }
}

