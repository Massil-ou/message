package main.a.profile;

import main.a.NavigationModel;
import main.a.profile.ProfileModel;
import main.java.com.ubo.tp.message.datamodel.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

public class ProfileView extends JPanel {
    private ProfileModel profileModel;
    private JTextField userTagTextField;
    private JTextField userNomTextField;
    private JTextField userPasswordTextField;
    private JTextField avatarPathTextField;
    private JTextField followsTextField;
    public ProfileView(NavigationModel model, ProfileModel menuModel) {
        this.profileModel = menuModel;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(Component.CENTER_ALIGNMENT);

        userTagTextField = new JTextField(20);
        userTagTextField.setEditable(false);
        userNomTextField = new JTextField(20);
        userNomTextField.setEditable(false);
        userPasswordTextField = new JTextField(20);
        userPasswordTextField.setEditable(false);
        avatarPathTextField = new JTextField(20);
        avatarPathTextField.setEditable(false);
        followsTextField = new JTextField(20);
        followsTextField.setEditable(false);

        JLabel userTagLabel = new JLabel("User Tag:");
        JLabel userNomLabel = new JLabel("User Nom:");
        JLabel userPasswordLabel = new JLabel("User Password:");
        JLabel avatarPathLabel = new JLabel("Avatar Path:");
        JLabel followsLabel = new JLabel("Follows:");

        add(userTagLabel);
        add(userTagTextField);
        add(userNomLabel);
        add(userNomTextField);
        add(userPasswordLabel);
        add(userPasswordTextField);
        add(avatarPathLabel);
        add(avatarPathTextField);

        JButton deconnexionButton = new JButton("Déconnecter");
        deconnexionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("LoginView");
            }
        });
        add(deconnexionButton);
        JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("MenuView");
            }
        });
        add(backButton);
        add(Box.createVerticalGlue());
        updateFields();
    }

    private void updateFields() {
        userTagTextField.setText(profileModel.getUser().getUserTag());
        userNomTextField.setText(profileModel.getUser().getName());
        userPasswordTextField.setText(profileModel.getUser().getUserPassword());
        avatarPathTextField.setText(profileModel.getUser().getAvatarPath());
        followsTextField.setText(profileModel.getUser().getFollows().toString());
    }
}
