package main.a.profile;


public interface ProfileObservableController {

    void addObserver(ProfileObserver observer);
    void removeObserver(ProfileObserver observer);
    void notifyObservers();


}

interface ProfileObserver {
    void navigateToSigninView();

}
