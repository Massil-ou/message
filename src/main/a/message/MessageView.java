package main.a.message;

import main.a.NavigationModel;
import main.a.message.MessageModel;
import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.datamodel.User;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

public class MessageView extends JPanel {
    private MessageModel messageModel;
    private JComboBox<User> userComboBox;
    private JTextField messageTextField;
    private JTextField resultTextField;
    private JList<String> messageList;

    public MessageView(NavigationModel model, MessageModel messageModel) {
        this.messageModel = messageModel;

        setLayout(new BorderLayout());

        JPanel topPanel = new JPanel(new BorderLayout());

        userComboBox = new JComboBox<>(messageModel.users.toArray(new User[0]));
        topPanel.add(userComboBox, BorderLayout.NORTH);

        messageTextField = new JTextField();
        topPanel.add(messageTextField, BorderLayout.CENTER);

        JButton sendButton = new JButton("Envoyer");
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String message = messageTextField.getText();
                if (message.length() <= 200 && message.length() >= 1) {
                    User selectedUser = userComboBox.getItemAt(userComboBox.getSelectedIndex());
                    messageModel.setMessage(selectedUser, message);
                    messageTextField.setText("");
                    if (messageModel.isValid()) {
                        resultTextField.setText("Envoi valide");
                        //model.navigateTo("Message");
                    } else {
                        resultTextField.setText("Envoi invalide");
                    }
                    messageModel.setValid(false);

                } else {
                    JOptionPane.showMessageDialog(MessageView.this,
                            "Le texte du message ne doit pas dépasser 200 caractères.",
                            "Erreur", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        topPanel.add(sendButton, BorderLayout.EAST);

        add(topPanel, BorderLayout.NORTH);

        JPanel bottomPanel = new JPanel(new BorderLayout());

        JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("MenuView");
            }
        });
        bottomPanel.add(backButton, BorderLayout.WEST);

        resultTextField = new JTextField(20);
        resultTextField.setEditable(false);
        bottomPanel.add(resultTextField, BorderLayout.CENTER);

        add(bottomPanel, BorderLayout.CENTER);

        messageList = new JList<>();
        JScrollPane scrollPane = new JScrollPane(messageList);
        bottomPanel.add(scrollPane, BorderLayout.SOUTH);

        // Ajout d'un écouteur à votre JList messageList
        messageList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    // Obtenez l'élément sélectionné dans la liste
                    String selectedMessage = messageList.getSelectedValue();

                    // Faites ce que vous voulez avec l'élément sélectionné, par exemple :
                    System.out.println("Message sélectionné : " + selectedMessage);
                }
            }
        });


        Set<Message> mMessages = this.messageModel.getMessage();

        DefaultListModel<String> listModel = new DefaultListModel<>();

        for (Message message : mMessages) {
            //if(message.getSender().equals(messageModel.getUser())){
            listModel.addElement(message.getText());
            // }
        }
        messageList.setModel(listModel);
    }
    public void updateMessageList(Set<Message> messages) {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        for (Message message : messages) {
            listModel.addElement(message.getText());
        }
        messageList.setModel(listModel);
    }
}
