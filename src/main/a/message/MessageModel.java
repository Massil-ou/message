package main.a.message;



import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.datamodel.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MessageModel implements MessageObservableController {


    protected User user;
    protected Set<User> users;

    protected String mUserNom;
    protected Set<Message> mMessages;
    protected String mUserTag;
    private String text;
    private Message sender;
    private Message recipient;
    private boolean isValid = false;

    private List<MessageObserver> observers = new ArrayList<>();
    public MessageModel(String userTag, String userPassword,Set<Message> mMessages) {
        mUserTag = userTag;
        mUserNom = userPassword;
        this.mMessages=mMessages;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    public Set<User> getUsers(Set<User> users) {
        return this.users;
    }

    public User getUsers() {
        return user;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
    public Set<Message> getMessage( ) {
        return  mMessages;
    }
    public void setMessage(Set<Message> newMessag ) {
          this.mMessages=newMessag;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
    public Message getSender() {
        return sender;
    }
    public Message getRecipient() {
        return recipient;
    }

    public void setMessage(User selectedUser, String text) {
        Message sender=new Message(user,text);
        this.sender = sender;

        Message recipient=new Message(selectedUser,text);
        this.recipient = recipient;
        send();
    }

    @Override
    public void addObserver(MessageObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(MessageObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (MessageObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }

    @Override
    public void send() {
        for (MessageObserver observer : observers) {
            observer.send();
        }
    }

}

