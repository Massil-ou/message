package main.a.message;


import main.a.NavigationModel;
import main.a.signin.SignInView;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;

public class MessageController implements MessageObserver {
    private NavigationModel model;
    private MessageModel messageModel;
    private MessageView messageView;
    protected IDatabase mDatabase;

    protected EntityManager mEntityManager;

    public MessageController(NavigationModel model, MessageModel messageModel, IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.messageModel=messageModel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.messageModel.addObserver(this);
    }
    public void setView(MessageView messageView){
        this.messageView=messageView;
    }

    public MessageView getView(){
        return messageView;
    }

    @Override
    public void navigateToSigninView() {
        model.navigateTo("SignInView");
    }

    @Override
    public void send() {
        this.mDatabase.addMessage(this.messageModel.getSender());
        messageModel.setMessage(this.mDatabase.getMessages());
        messageModel.setValid(true); // L'inscription est valide
        messageView.updateMessageList(this.mDatabase.getMessages());
    }

}