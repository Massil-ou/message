package main.a.message;


public interface MessageObservableController {

    void addObserver(MessageObserver observer);
    void removeObserver(MessageObserver observer);
    void notifyObservers();
    void send();


}

interface MessageObserver {
    void navigateToSigninView();
    void send();

}
