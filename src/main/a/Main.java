package main.a;

import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.Database;
import main.java.com.ubo.tp.message.core.database.IDatabase;

public class Main {
    public static void main(String[] args) {
        IDatabase database = new Database();
        EntityManager entityManager = new EntityManager(database);

        NavigationModel model = new NavigationModel();
        MainController mainController = new MainController(model,database,entityManager);
    }
}