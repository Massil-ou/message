package main.a.filterMessage;

import main.a.NavigationModel;
import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.datamodel.User;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

public class FilterMessageView extends JPanel {
    private FilterMessageModel filterMessageModel;
    private JComboBox<User> userComboBox;
    private JTextField messageTextField;
    private JTextField resultTextField;
    private JList<String> messageList;

    public FilterMessageView(NavigationModel model, FilterMessageModel filterMessageModel) {
        this.filterMessageModel = filterMessageModel;

        setLayout(new BorderLayout());

        JPanel topPanel = new JPanel(new BorderLayout());

        userComboBox = new JComboBox<>(filterMessageModel.users.toArray(new User[0]));
        topPanel.add(userComboBox, BorderLayout.NORTH);

        messageTextField = new JTextField();
        topPanel.add(messageTextField, BorderLayout.CENTER);

        JButton sendButton = new JButton("Recherche");
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String message = messageTextField.getText();
                if (message.length() <= 200 && message.length() >= 1) {
                    User selectedUser = userComboBox.getItemAt(userComboBox.getSelectedIndex());

                    if(message.contains("@")){
                        filterMessageModel.setSearch1(message);
                    }else if(message.contains("#")){
                        filterMessageModel.setSearch2(message);
                    }else {
                        filterMessageModel.setSearch3(message);
                    }
                } else {
                    JOptionPane.showMessageDialog(FilterMessageView.this,
                            "Le texte du rechreche ne doit pas dépasser 10 caractères.",
                            "Erreur", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        topPanel.add(sendButton, BorderLayout.EAST);

        add(topPanel, BorderLayout.NORTH);

        JPanel bottomPanel = new JPanel(new BorderLayout());

        JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.navigateTo("MenuView");
            }
        });
        bottomPanel.add(backButton, BorderLayout.WEST);

        resultTextField = new JTextField(20);
        resultTextField.setEditable(false);
        bottomPanel.add(resultTextField, BorderLayout.CENTER);

        add(bottomPanel, BorderLayout.CENTER);

        messageList = new JList<>();
        JScrollPane scrollPane = new JScrollPane(messageList);
        bottomPanel.add(scrollPane, BorderLayout.SOUTH);

        // Ajout d'un écouteur à votre JList messageList
        messageList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    // Obtenez l'élément sélectionné dans la liste
                    String selectedMessage = messageList.getSelectedValue();

                    // Faites ce que vous voulez avec l'élément sélectionné, par exemple :
                    System.out.println("Message sélectionné : " + selectedMessage);
                }
            }
        });


        Set<Message> mMessages = this.filterMessageModel.getMessage();

        DefaultListModel<String> listModel = new DefaultListModel<>();

        for (Message message : mMessages) {
            listModel.addElement(message.getText());
        }
        messageList.setModel(listModel);
    }
    public void updateMessageList(Set<Message> messages) {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        for (Message message : messages) {
            listModel.addElement(message.getText());
        }
        messageList.setModel(listModel);
    }
}
