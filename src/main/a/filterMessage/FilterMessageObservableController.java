package main.a.filterMessage;


public interface FilterMessageObservableController {

    void addObserver(FilterMessageObserver observer);
    void removeObserver(FilterMessageObserver observer);
    void notifyObservers();
    void search1();

    void search2();
    void search3();

}

interface FilterMessageObserver {
    void navigateToSigninView();
    void search1();
    void search2();
    void search3();

}
