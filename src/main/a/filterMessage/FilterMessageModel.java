package main.a.filterMessage;



import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.datamodel.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FilterMessageModel implements FilterMessageObservableController {


    protected User user;

    protected String mUserNom;
    protected Set<Message> mMessages;
    protected String mUserTag;
    private String text;
    protected Set<User> users;

    private Message sender;
    private Message recipient;
    private boolean isValid = false;

    private List<FilterMessageObserver > observers = new ArrayList<>();
    public FilterMessageModel(String userTag, String userPassword,Set<Message> mMessages) {
        mUserTag = userTag;
        mUserNom = userPassword;
        this.mMessages=mMessages;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    public Set<User> getUsers(Set<User> users) {
        return this.users;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }
    public Set<Message> getMessage( ) {
        return  mMessages;
    }
    public void setMessage(Set<Message> newMessag ) {
          this.mMessages=newMessag;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
    public Message getSender() {
        return sender;
    }
    public Message getRecipient() {
        return recipient;
    }


    public String search1;
    public String search2;
    public String search3;

    public void setSearch1(String search1){
        this.search1=search1;
        search1();
    }
    public void setSearch2(String search2){
        this.search2=search2;
        search2();
    }
    public void setSearch3(String search3){
        this.search3=search3;
        search3();

    }
    public void setMessage(User selectedUser, String text) {
        Message sender=new Message(user,text);
        this.sender = sender;

        Message recipient=new Message(selectedUser,text);
        this.recipient = recipient;
        search1();
    }

    @Override
    public void addObserver(FilterMessageObserver observer) {
        observers.add(observer);

    }

    @Override
    public void removeObserver(FilterMessageObserver observer) {

    }

    @Override
    public void notifyObservers() {
        for (FilterMessageObserver observer : observers) {
            observer.navigateToSigninView();
        }
    }

    @Override
    public void search1() {
        for (FilterMessageObserver observer : observers) {
            observer.search1();
        }
    }

    @Override
    public void search2() {
        for (FilterMessageObserver observer : observers) {
            observer.search2();
        }
    }

    @Override
    public void search3() {
        for (FilterMessageObserver observer : observers) {
            observer.search3();
        }
    }

}

