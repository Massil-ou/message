package main.a.filterMessage;


import main.a.NavigationModel;
import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.Message;

import java.util.HashSet;
import java.util.Set;

public class FilterMessageController implements FilterMessageObserver {
    private NavigationModel model;
    private FilterMessageModel filterMessageModel;
    private FilterMessageView filtermessageView;

    protected IDatabase mDatabase;
    protected EntityManager mEntityManager;

    public FilterMessageController(NavigationModel model, FilterMessageModel messageModel, IDatabase database, EntityManager entityManager) {
        this.model = model;
        this.filterMessageModel=messageModel;
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.filterMessageModel.addObserver(this);
    }
    public void setView(FilterMessageView filtermessageView){
        this.filtermessageView=filtermessageView;
    }

    public FilterMessageView getView(){
        return filtermessageView;
    }

    @Override
    public void navigateToSigninView() {
        model.navigateTo("SignInView");
    }

    @Override
    public void search1() {
        Set<Message> messages=this.mDatabase.getMessages();
        Set<Message> result = new HashSet<>();

        String chaine = filterMessageModel.search1;
        chaine = chaine.replace("@", ""); // Retire le caractère '@' de la chaîne

        for (Message message : messages) {
            if(message.getText().contains(filterMessageModel.search1) ||
            message.getSender().getName().equals(chaine)
            ){
                result.add(message);
            }
        }
        filterMessageModel.setMessage(result);
        filterMessageModel.setValid(true);
        filtermessageView.updateMessageList(result);

    }

    @Override
    public void search2() {

        Set<Message> messages=this.mDatabase.getMessages();
        Set<Message> result = new HashSet<>();

        for (Message message : messages) {
            if(message.getText().contains(filterMessageModel.search2)){
                result.add(message);
            }
        }
        filterMessageModel.setMessage(result);
        filterMessageModel.setValid(true);
        filtermessageView.updateMessageList(result);
    }

    @Override
    public void search3() {

        Set<Message> messages=this.mDatabase.getMessages();
        Set<Message> result = new HashSet<>();

        String chaine = filterMessageModel.search3;
        chaine = chaine.replace("@", ""); // Retire le caractère '@' de la chaîne
        for (Message message : messages) {
            if(message.getText().contains(chaine)){
                result.add(message);
            }
        }
        filterMessageModel.setMessage(result);
        filterMessageModel.setValid(true);
        filtermessageView.updateMessageList(result);

    }

}